module.exports = {
  // Auto discover
  autodiscover: true,
  autodiscoverNamespaces: [
    "gitlab-com/gl-infra",
    "gitlab-org",
  ],
  autodiscoverTopics: [
    "managed-by-soos"
  ],

  // Do not onboard
  onboarding: false,

  // Require config in repo
  requireConfig: "required",

  // Host rules as ENV vars - https://docs.renovatebot.com/self-hosted-configuration/#detecthostrulesfromenv
  detectHostRulesFromEnv: true,

  // Allow all post-upgrade commands
  allowPostUpgradeCommandTemplating: true,
  allowedPostUpgradeCommands: [".*"],
};
