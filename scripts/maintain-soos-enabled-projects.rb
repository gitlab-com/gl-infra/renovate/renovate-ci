#!/usr/bin/env ruby
# frozen_string_literal: true

require 'net/http'
require 'json'

TOKEN = ENV.fetch('RENOVATE_TOKEN')
GITLAB_API = ENV.fetch('CI_API_V4_URL', 'https://gitlab.com/api/v4')
IGNORED_PATHS = [].freeze
SOOS_TOPIC = 'managed-by-soos'

def with_gitlab_connection(&block)
  uri = URI(GITLAB_API)
  Net::HTTP.start(uri.host, uri.port, use_ssl: true, &block)
end

def gitlab_get(conn, path, params = {})
  raise 'path needs to start with `/`' unless path.start_with?('/')

  warn "-- GET #{GITLAB_API}#{path}"

  params_query = URI.encode_www_form(params.merge(private_token: TOKEN))
  url = "#{GITLAB_API}#{path}?#{params_query}"

  conn.get(url)
end

def gitlab_put(conn, path, query={}, params={})
  warn "-- PUT #{GITLAB_API}#{path}: #{params.to_json}"
  params_query = URI.encode_www_form(query.merge(private_token: TOKEN))
  url = "#{GITLAB_API}#{path}?#{params_query}"

  conn.put(url, params.to_json, {'content-type': 'application/json'})
end

GL_INFRA_GROUP_ID = with_gitlab_connection do |conn|
  gl_infra = URI.encode_uri_component('gitlab-com/gl-infra')
  response = gitlab_get(conn, "/groups/#{gl_infra}")

  JSON.parse(response.body).fetch('id')
end

def project_has_renovate_schedule?(project, conn, project_path)
  return false unless project['jobs_enabled']

  result = gitlab_get(conn, "/projects/#{project['id']}/pipeline_schedules")
  schedules = JSON.parse(result.body)
  schedules.any? { |schedule| schedule['description'].downcase.include?('renovate') }
rescue StandardError => e
  warn "⚠ Couldn't load schedules for #{project_path}: #{e.inspect}\n#{result&.body&.inspect}"
  false
end

def project_has_renovate_config?(project, conn, project_path)
  result = gitlab_get(conn, "/projects/#{project['id']}/repository/files/renovate.json", ref: project['default_branch'])

  result.is_a?(Net::HTTPSuccess)
rescue StandardError => e
  warn "⚠ Couldn't load renovate config for #{project_path}: #{e.inspect}\n#{result&.body&.inspect}"
  false
end

def handle_page(response)
  projects = JSON.parse(response.body)
  warn "--- Handling page: #{response.header['x-page']} (#{projects.size} projects)"

  global_renovate = []
  common_ci_tasks_renovate = []
  ignored = []
  no_renovate = []
  with_gitlab_connection do |conn|
    projects.each do |p|
      project_path = p['path_with_namespace']
      if IGNORED_PATHS.any? { |ignored_path| project_path.starts_with?(ignored_path) }
        ignored << p
      elsif !project_has_renovate_config?(p, conn, project_path)
        no_renovate << p
      elsif project_has_renovate_schedule?(p, conn, project_path)
        common_ci_tasks_renovate << p
        remove_soos_topic(p, conn)
      else
        global_renovate << p
        add_soos_topic(p, conn)
      end
    end
  end

  {
    global_renovate:,
    common_ci_tasks_renovate:,
    no_renovate:,
    ignored:
  }
end

def add_soos_topic(p, conn)
  topics = p['topics']

  if p['topics'].include?(SOOS_TOPIC)
    warn "-- #{p['path_with_namespace']} already managed-by-soos"
    return
  end

  topics += [SOOS_TOPIC]

  gitlab_put(conn, "/projects/#{p['id']}", {}, { topics: topics.join(',') } )
end

def remove_soos_topic(p, conn)
  topics = p['topics']

  unless p['topics'].include?(SOOS_TOPIC)
    warn "-- #{p['path_with_namespace']} not managed-by-soos"
    return
  end

  topics -= [SOOS_TOPIC]

  gitlab_put(conn, "/projects/#{p['id']}", {}, { topics: topics.join(',') } )
end

def main
  resulting_hash = {}

  with_gitlab_connection do |conn|
    result = gitlab_get(conn, "/groups/#{GL_INFRA_GROUP_ID}/projects", include_subgroups: 'true')

    threads = []
    threads << Thread.new do
      Thread.current[:result] = handle_page(result)
    end

    while result.header['x-next-page'] && result.header['x-next-page'].to_i > 1
      result = gitlab_get(conn, "/groups/#{GL_INFRA_GROUP_ID}/projects", include_subgroups: 'true',
                                                                         page: result.header['x-next-page'])

      threads << Thread.new do
        Thread.current[:result] = handle_page(result)
      end

      running_threads = threads.select(&:alive?)
      warn "-- Running threads: #{running_threads.size}"
      sleep(0.01) until running_threads.size < 10
    end

    results = threads.map(&:join).map { |t| t[:result] }

    resulting_hash = results.inject({}) do |memo, page_result|
      memo.merge(page_result) { |_, previous, new_page| previous + new_page }
    end

    warn "- #{resulting_hash.values.map(&:size).sum} projects: #{resulting_hash.transform_values(&:size).inspect}"
  end

  puts JSON.pretty_generate(resulting_hash.transform_values { |projects| projects.map { |project| project['path_with_namespace'] } })
end

main
