#!/usr/bin/env ruby

require 'gitlab'
require 'json'
require 'time'
require 'uri'
require 'yaml'

GITLAB_API = ENV.fetch('CI_API_V4_URL', 'https://gitlab.com/api/v4')
PROJECTS = YAML.load_file(ENV['PROJECTS_YAML'])
SLACK_WEBHOOK_URL = ENV.fetch('SLACK_WEBHOOK_URL', nil)
TOKEN = ENV.fetch('RENOVATE_TOKEN')

class Notifier

  def initialize(projects)
    @client = Gitlab.client(endpoint: GITLAB_API, private_token: TOKEN)
    @projects = projects
  end

  def old_mrs_blocks(name, mrs)
    [
      {
        type: "header",
        text: {
          type: "plain_text",
          text: "#{name}:"
        }
      },
    ] + mrs.map { |mr|
      {
        type: "section",
        text: {
          type: "mrkdwn",
          text: "<#{mr.web_url}|##{mr.iid} - #{mr.title}>"
        }
      }
    }
  end

  def send_slack_alert(old_mrs)
    message = {
      blocks: [
        {
          type: "header",
          text: {
            type: "plain_text",
            text: ":frog-alert: There are :renovate: Renovate MRs older than 1 month: :alert: :tanuki-sparkle-2:"
          }
        }
      ] + old_mrs.map { |name, mrs| old_mrs_blocks(name, mrs) }.flatten
    }

    puts JSON.pretty_generate(message)
    HTTParty.post(SLACK_WEBHOOK_URL, body: message.to_json, headers: { 'Content-Type' => 'application/json' }) if SLACK_WEBHOOK_URL
  end

  def old_project_mrs(id)
    @client.merge_requests(id, {
      labels: 'dependencies',
      state: 'opened',
      created_before: (DateTime.now - 30).iso8601,
      order_by: 'created_at',
      sort: 'desc',
    })
  rescue Gitlab::Error::ResponseError => e
    $stderr.puts e
    []
  end

  def old_group_mrs(id)
    @client.group_merge_requests(URI.encode_uri_component(id), {
      labels: 'dependencies',
      state: 'opened',
      created_before: (DateTime.now - 30).iso8601,
      order_by: 'created_at',
      sort: 'desc',
    })
  rescue Gitlab::Error::ResponseError => e
    $stderr.puts e
    []
  end

  def run!
    old_mrs = {}

    @projects['projects'].each do |project_id|
      mrs = old_project_mrs(project_id)
      next if mrs.empty?
      old_mrs[@client.project(project_id).name] = mrs
    end

    @projects['groups'].each do |group_id|
      mrs = old_group_mrs(group_id)
      next if mrs.empty?
      old_mrs[@client.group(group_id).name] = mrs
    end

    send_slack_alert(old_mrs) unless old_mrs.empty?
  end

end

Notifier.new(PROJECTS).run!
