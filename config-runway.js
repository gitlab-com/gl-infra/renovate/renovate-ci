module.exports = {
  // Auto discover
  autodiscover: true,
  autodiscoverNamespaces: [
    "gitlab-com/gl-infra",
    "gitlab-org",
  ],
  autodiscoverTopics: [
    "runway-workloads"
  ],

  onboarding: true,
  onboardingConfig: {
    "$schema": "https://docs.renovatebot.com/renovate-schema.json",
    "extends": [
      "gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-common",
      "gitlab>gitlab-com/gl-infra/common-ci-tasks:renovate-truncated-versions",
      "gitlab>gitlab-com/gl-infra/platform/runway/runwayctl:renovate-runway"
    ]
  },

  // Require config in repo
  // See https://docs.renovatebot.com/self-hosted-configuration/#requireconfig for behaviour with onboarding: true
  requireConfig: "required",

  // Host rules as ENV vars - https://docs.renovatebot.com/self-hosted-configuration/#detecthostrulesfromenv
  detectHostRulesFromEnv: true,

  // Allow all post-upgrade commands
  allowPostUpgradeCommandTemplating: true,
  allowedPostUpgradeCommands: [".*"],
};
