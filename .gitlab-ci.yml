---
stages:
  - build
  - test
  - deploy
  - notify

include:
  - project: gitlab-com/gl-infra/renovate/renovate-runner
    file: /templates/renovate.gitlab-ci.yml
    ref: v20.1.0

variables:
  # Renovate
  RENOVATE_REQUIRE_CONFIG: required
  RENOVATE_ONBOARDING: 'false'
  LOG_LEVEL: info

  RENOVATE_GL_HOST: gitlab-com

  RENOVATE_HOST_RULES: >
    [
      {"hostType": "docker", "matchHost": "registry.ops.gitlab.net","username": "${HELM_OPS_GITLAB_NET_USERNAME}","password": "${HELM_OPS_GITLAB_NET_PASSWORD}"},
      {"hostType": "helm", "matchHost": "https://registry.ops.gitlab.net/gitlab-com/gl-infra/charts","username": "${HELM_OPS_GITLAB_NET_USERNAME}","password": "${HELM_OPS_GITLAB_NET_PASSWORD}"}
    ]

.id_tokens:
  id_tokens:
    VAULT_ID_TOKEN:
      aud: https://vault.gitlab.net

.renovate_token:
  secrets:
    RENOVATE_TOKEN:
      file: false
      vault: ${VAULT_SECRETS_PATH}/${CI_ENVIRONMENT_NAME}/${RENOVATE_GL_HOST}/token@ci

maintain-renovate-projects:
  extends:
    - .id_tokens
    - .renovate_token
  stage: build
  needs: []
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" || $CI_PIPELINE_SOURCE == "web"
      when: manual
  allow_failure: true
  tags:
    - gitlab-org
  environment:
    name: readonly
    action: verify
  image: ruby:3.4.2
  script:
    - ruby scripts/maintain-soos-enabled-projects.rb

renovate:
  extends:
    - .id_tokens
    - .renovate
    - .renovate_token
  resource_group: production
  environment:
    name: renovate
  tags:
    - gitlab-org
  timeout: 2h
  rules:
    - if: $RENOVATE_NOTIFY
      when: never
    # Schedule
    - if: $CI_PIPELINE_SOURCE == "schedule"
    # Manual web triggers
    - if: $CI_PIPELINE_SOURCE == "web"
  secrets:
    GITHUB_COM_TOKEN:
      file: false
      vault: renovate/github/token@shared
    # Host rules - https://docs.renovatebot.com/self-hosted-configuration/#detecthostrulesfromenv
    # Terraform modules gitlab.com
    TERRAFORM__MODULE_GITLAB_COM_TOKEN:
      file: false
      vault: gitlab-com/terraform/modules-registry-ro/token@shared
    # Terraform modules ops.gitlab.net
    TERRAFORM__MODULE_OPS_GITLAB_NET_TOKEN:
      file: false
      vault: ops-gitlab-net/terraform/modules-registry-ro/token@shared
    # Helm registry ops.gitlab.net
    HELM_OPS_GITLAB_NET_USERNAME:
      file: false
      vault: access_tokens/ops-gitlab-net/gitlab-com/gl-infra/_group_access_tokens/renovate-oci-readonly/username@ci
    HELM_OPS_GITLAB_NET_PASSWORD:
      file: false
      vault: access_tokens/ops-gitlab-net/gitlab-com/gl-infra/_group_access_tokens/renovate-oci-readonly/token@ci

renovate:dry-run:
  extends:
    - .id_tokens
    - .renovate
    - .renovate_token
  stage: test
  needs: []
  resource_group: production
  environment:
    name: readonly
    action: verify
  tags:
    - gitlab-org
  timeout: 2h
  secrets: !reference [renovate, secrets]
  script:
    - renovate --dry-run=full $RENOVATE_EXTRA_FLAGS
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - config*.js

.runway:
  variables:
    RENOVATE_CONFIG_FILE: config-runway.js
    RENOVATE_ONBOARDING: 'true'
    RENOVATE_ONBOARDING_CONFIG: null

renovate-runway:
  extends:
    - .runway
    - renovate

renovate-runway:dry-run:
  extends:
    - .runway
    - renovate:dry-run

renovate:notify:
  extends:
    - .id_tokens
    - .renovate_token
  stage: deploy
  environment:
    name: readonly
    action: verify
  needs: []
  variables:
    PROJECTS_YAML: projects/${RENOVATE_GL_HOST}.yaml
  secrets:
    SLACK_WEBHOOK_URL:
      file: false
      vault: renovate/notifier/slack_webhook_url@shared
  tags:
    - gitlab-org
  image: ruby:3.4.2
  script:
    - bundle install
    - ruby scripts/notify.rb
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule" && $RENOVATE_NOTIFY
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        paths:
          - .gitlab-ci.yml
          - Gemfile
          - Gemfile.lock
          - projects/*
          - scripts/notify.rb
      when: manual
      allow_failure: true
